# print-fst

## Installation

Note: `unbuffer` should be present in PATH.

### By cloning this repo
To put `fst2tex` in PATH, git clone and run: `npm install -g`.

Otherwise, git clone, `npm install` and then `node main.js`

### Or directly
`npx -p git+https://gitlab.inria.fr/lfrances/print-fst fst2tex`
