#!/usr/bin/env node

const NAME  = "NAME";
const SPACE = "SPACE";
const OP    = "OP";
const OPEN_TAG = "OPEN_TAG";
const CLOSE_TAG = "CLOSE_TAG";
const LATEX = "LATEX";
const fs = require('fs');

let gmatch = (src, re, f) => {
    let collect = [];
    src.replace(re, (...params) => collect.push(f(...params)));
    return collect;
};

let gmatchk = (src, re, f) => {
    let collect = {};
    src.replace(re, (...params) => {
	let [k, v] = f(...params);
	collect[k.trim()] = v;
    });
    return collect;
};

let emacscli = 'unbuffer emacs -nw -q --load "fstar-mode.el/fstar-mode.el"';

let fst_to_html_emacs = (list, tmp = n => `/tmp/tmp_print_${n}.fst`) => {
    const execSync = require('child_process').execSync;
    for(let i in list)
	fs.writeFileSync(tmp, list[i].source); // tsdh-light
    // --eval="(dolist (i custom-enabled-themes) (disable-theme i))"\
    let arg = `${emacscli} ${list.map(x => '"'+x.source+'"')} \
 --eval="(load-theme 'tsdh-light)"\
 ${list.map(x => `
    --eval="(htmlfontify-buffer)"\
    --eval="(write-file \\"${x.filename}.html\\")"\
    --eval="(kill-buffer)"\
    --eval="(kill-buffer)"\
    `
)} --eval="(kill-emacs)"
`;
 // --eval="(switch-to-buffer (other-buffer))"\
    let r = execSync(arg);
};

let fst_to_html = (src, tmp = '/tmp/tmp_print.fst') => {
    if(true){
	const execSync = require('child_process').execSync;
	fs.writeFileSync(tmp, src); // tsdh-light
 // --eval="(dolist (i custom-enabled-themes) (disable-theme i))"\
	let arg = `${emacscli} "${tmp}" \
 --eval="(load-theme 'tsdh-light)"\
 --eval="(htmlfontify-buffer)"\
 --eval="(write-file \\"${tmp}.html\\")"\
 --eval="(switch-to-buffer (other-buffer))"\
 --eval="(kill-emacs)"
`;
	let r = execSync(arg);
    }
    let cheerio = require("cheerio");
    let $ = cheerio.load(''+fs.readFileSync(tmp+'.html'));

    let list = $('body > pre')[0].children;

    let res = [];
    let pos = 0;
    for(let x of list){
	res.push([pos, $(x).attr('class')]);
	pos += $(x).text().length;
    }
    res = res.reverse();
    let css_raw = $('head style').html();
    let css = gmatchk(css_raw, /span.([^ ]+) *{([^}]+)}/g,
			      (_, name, rules) =>
			      [name,
			       gmatchk(
				   rules,
				   /([\w-]+):([^;]*);/g,
				   (_, name, body) => [name, body.trim()])
			      ]
			     );
    return {
	getClass: p => res.find(([v,_]) => v < p)[1],
	head: $('head').html(),
	css
    };
};

let contains = (list, something) => list.indexOf(something) != -1;

// let mkName = str => ({content: str, kind: NAME});
// let mkBox = (tag, content) => ({content, kind: BOX, tag});
// let mkOp = str => ({content: str, kind: OP});
let kindOfChar = (ch, prev_kind) => {
    ch = ch.toLowerCase();
    let is = chars => contains(chars.split(''), ch);
    return is('abcdefghijklmnopqrstuvwxyz1234567890#_\'`$')
	? NAME
	: is('.') && prev_kind == NAME
	? NAME
	: is('[({')
	? OPEN_TAG
	: is('})]')
	? CLOSE_TAG
	: is(' \t\n')
	? SPACE
	: OP;
};

let tokenize = (input, getClassAt) => {
    let mode = NAME;
    let tokens = [];
    let cur = '';
    let pos = 0;
    let oldMode;
    let push = (kind) => {
	cur && tokens.push({str: cur, kind, cclass: getClassAt(pos-1)});
	cur = '';
    };
    while(pos < input.length){
	let ch = input[pos];
	pos++;
	let nCh = input[pos];
	if(ch=='"' || (ch=="/" && nCh=="/")){
	    push(mode);
	    cur += ch + (ch=="/" ? "/" : "");
	    let matching_char = ch=='/' ? '\n' : '"';
	    while(input[pos++] != matching_char && pos < input.length)
		cur += input[pos];
	    cur += ch=="/" ? "" : '"';
	    push(ch);
	    continue;
	}
	if(kindOfChar(ch, oldMode) != mode || mode==OPEN_TAG || mode==CLOSE_TAG){
	    push(mode);
	    mode = kindOfChar(ch, oldMode);
	}
	cur += ch;
	oldMode = mode;
    }
    push(mode);
    return tokens;
};

let parse = (tokens, pos=0) => {
    let AST = [];
    let stack = [];
    let current = [];
    let push = () => {stack.push(current); current = [];};
    let pop = () => {let last = stack.pop();
		     last.push(current);
		     current = last;};
    while(pos < tokens.length){
	if(tokens[pos].kind == OPEN_TAG)
	    push();
	current.push(tokens[pos]);
	if(tokens[pos].kind == CLOSE_TAG)
	    pop();
	pos++;
    }
    return current;
};

// node rule = [node] -> [node]; text rule = token -> token 
let latexify = (nodeRules, textRules, ast) => {
    let applyRules = rules => input => 
	rules.reduce((input, rule) => rule(input) || input, input);
    let map = nodes => 
	nodes instanceof Array
	? applyRules(nodeRules)(nodes.map(node => map(node)))
	: applyRules(textRules)(nodes);
    return map(ast);
};
let tRulesMakers = {
    replaceOperator: (op, newstr) => ({kind, cclass, str}) => str==op && ({str: newstr, kind, cclass}),
    replaceName: (name, newstr) => ({kind, cclass, str}) => str==name && ({str: newstr, kind, cclass}),
    replaceSubstr: (needle, replace) => ({kind, cclass, str}) => str.match(needle) && ({str: str.replace(needle, replace), kind, cclass})
};
let onNodes = f => x => x instanceof Array && f(x);
let onNode = f => x => !(x instanceof Array) && f(x);
let onNodeOfKind = (k, f) => x => !(x instanceof Array) && x.kind==k && f(x.str);
let wrapInArray = o => o instanceof Array ? o : [o];
let nRulesMakers = {
    generic: (discr, n_before, n_after, f) => nodes => {
	let i = nodes.findIndex(discr);
	if(i == -1)
	    return nodes;
	let inner = nodes.slice(i-n_before, 1+i+n_before+n_after);
	inner = inner.filter(x => x.kind != SPACE);
	return [...nodes.slice(0, i-n_before),
		...[f(...inner)],
		...nodes.slice(1+i+n_before+n_after)
	       ];
    },
    a_op_b: (op, f) =>
	nRulesMakers.generic(onNodeOfKind(OP, mop => mop==op), -1, 1, f),
    f_a_b_: (txt, f, n=f.length) =>
	nRulesMakers.generic(onNodeOfKind(NAME, t => t==txt), 0, n, f)
};

let skip = {kind: 'SKIP', str:''};
let removeComments = ({kind, str}) => kind=='/' && skip;

let printAST = (ast, render = (c, s) => `<span class=${c}>${s}</span>`) => {
    return ast.map(x => x instanceof Array
		   ? printAST(x, render)
		   : render(x.cclass||'', x.str)
		  ).join('');
};

let tokenizeThenParse = (source) => {
    let htmlStuff = fst_to_html(source);
    let tokens = tokenize(source, htmlStuff.getClass);
    let ast = parse(tokens);
    return {
	ast, tokens, htmlHead: htmlStuff.head, css: htmlStuff.css
    };
};


let stuff_replace = [["exists","∃"],["forall","∀"],["fun","λ"],["nat","ℕ"],["int","ℤ"],["True","⊤"],["False","⊥"],["*","×"],[":=","≔"],["::","⸬"],["<=","≤"],[">=","≥"],["<>","≠"],["/","∧"],["/","∨"],["~","¬"],["||","‖"],["<==>","⟺"],["==>","⟹"],["->","→"],["~>","↝"],["=>","⇒"],["<-","←"],["<--","⟵"],["-->","⟶"],["<<","≪"],["<|","◃"],["|>","▹"],["'a","α"],["'b","β"],["'c","γ"],["'d","δ"],["'e","ε"],["'f","φ"],["'g","χ"],["'h","η"],["'i","ι"],["'k","κ"],["'m","μ"],["'n","ν"],["'p","π"],["'q","θ"],["'r","ρ"],["'s","σ"],["'t","τ"],["'u","ψ"],["'w","ω"],["'x","ξ"],["'z","ζ"]];

let stuff_replace_tex = [
    ["exists","$\\exists$"],
    ["forall","$\\forall$"],
    ["fun","$\\lambda$"],
    ["nat","$\\mathbb N$"],
    ["int","$\\mathbb Z$"],
    ["True","$\\top$"],
    ["False","$\\bot$"],
    ["*","$\\times$"],
    [":=",":="],
    ["::","::"],
    ["<","$\\le$"],
    [">","$\\ge$"],
    ["<=","$\\leq$"],
    [">=","$\\geq$"],
    ["<>","$\\neq$"],
    ["/\\","$\\wedge$"],
    ["\\/","$\\∨ee$"],
    ["~","$\\neg$"],
    ["||","\\textbardbl\\;"],
    ["<==>","$\\Leftrightarrow$"],
    ["==>","$\\Rightarrow$"],
    ["->","$\\to$"],
    ["~>","$\\leadsto$"],
    ["=>","$\\implies$"],
    ["<-","$\\leftarrow$"],
    ["<--","$\\longleftarrow$"],
    ["-->","$\\longrightarrow$"],
    ["<<","$\\ll$"],
    ["<|","◃"],
    ["|>","▹"],
    ["'a","$\\alpha{}$"],//α
    ["'b","$\\beta{}$"],//β
    ["'c","$\\gamma{}$"],//γ
    ["'d","$\\delta{}$"],//δ
    ["'e","$\\epsilon{}$"],//ε
    ["'f","$\\phi{}$"],//φ
    ["'g","$\\chi{}$"],//χ
    ["'h","$\\eta{}$"],//η
    ["'i","$\\iota{}$"],//ι
    ["'k","$\\kappa{}$"],//κ
    ["'m","$\\mu{}$"],//μ
    ["'n","$\\nu{}$"],//ν
    ["'p","$\\pi{}$"],//π
    ["'q","$\\theta{}$"],//θ
    ["'r","$\\rho{}$"],//ρ
    ["'s","$\\sigma{}$"],//σ
    ["'t","$\\tau{}$"],//τ
    ["'u","$\\psi{}$"],//ψ
    ["'w","$\\omega{}$"],//ω
    ["'x","$\\xi{}$"],//ξ
    ["'z","$\\zeta{}$"],//ζ
    ["{","\\{"],
    ["}","\\}"]];
let wrapTex = str => ({str, kind: LATEX});

let printHTML = (ast, html_head) => {
    return `<html>${html_head}<body><pre>${printAST(ast)}</pre></body></html>`;
};

let omapl = (o, f) => Object.keys(o).map(k => f(k, o[k]));

let printLaTeX = (ast, css) => {
    let latexCmdOf = s => 'hlFACE'+s.replace(/[^\w]/g, '');
    let nothing = _ => undefined;
    let propTranslator = {
	color: hex => `textcolor[HTML]{${hex.slice(1).toUpperCase()}}`,
	'font-weight': q => ({
	    '700': 'textbf'
	})[q],
	'font-style': q => ({italic: 'textit'})[q],
	'text-decoration': q => q == 'underline'
	    && 'underline'
    };
    let macros = omapl(css, (rule_name, props) => {
	let latexCommand = latexCmdOf(rule_name);
	let command = omapl(props, (k, v) => (propTranslator[k]||nothing)(v))
	    .filter(x => x)
	    .reduce((acc, cur) => '\\' + cur + `{${acc}}`, '#1');
	return `\\newcommand{\\${latexCommand}}[1]{${command}}`;
    });
    let unmacros = omapl(css, (rule_name, _) => `\\undef\\${latexCmdOf(rule_name)}`);
    let f = s => s
	// .replace(/([{_}\\#])/g, '\\$1')
	// .replace(/->/g, '$\\to$')
	.replace(/\n/g, '~\\\\');
    return `{%
${macros.join('\n')}%
\\newenvironment{allintypewriter}{\\ttfamily}{}%
\\begin{allintypewriter}${printAST(ast, (c, s) => 
  (c && s.trim()!='')
  ? '\\'+latexCmdOf(c)+'{'+f(s)+'}'
  : f(s)
  ).replace(/\\\\( +)/g, (_, x) => '\\\\\\phantom{'+x.replace(/./g, 'a')+'}')
}%
\\end{allintypewriter}%
${unmacros.join('\n')}%
}%`;
};

let countUntil = (l, f) =>
    l.length
    ? ( f(l[0]) ? 1 + countUntil(l.slice(1), f) : 0 )
    : 0;

let trimLeftAST = l => 
    l.length && l[0].kind == SPACE
    ? trimLeftAST(l.slice(1))
    : l;

let trimAST = l => trimLeftAST(trimLeftAST(l).reverse()).reverse();

let skipf = (l, f) => l.length ? (f(l[0]) ? l.slice(1) : l) : l;

let flatten = (ast) =>
    ast
    .map(x => x instanceof Array ? flatten(x) : [x])
    .reduce((acc, l) => [...acc, ...l], []);

let deduplicateSpaces = (ast, wasSpace = false, old = null) => {
    // let nast = [];
    // for(let o of ast)
    // 	if(o instanceof Array){
    // 	    let res = deduplicateSpaces(o, wasSpace, old);
    // 	    if(res.length)
    // 		nast.push(res);
    // 	}else if(o.kind == SPACE){
    // 	    if(!wasSpace){
    // 		nast.push(o);
    // 		old = o;
    // 	    }else if(old){
    // 		// old.str += o.str;
    // 	    }
    // 	    wasSpace = true;
    // 	}else{
    // 	    old = null;
    // 	    wasSpace = false;
    // 	    nast.push(o);
    // 	}
    return ast;
};

let isKW = str => ["val", "let", "type", "and", "instance", "class"].indexOf(str) != -1;
let isQUAL = str => ["assume", "unfold", "abstract", "private", "noeq"].indexOf(str) != -1;
let findTopLevel = (ast, name, start=0) => {
    ast = deduplicateSpaces(ast);
    let f = (name, i = 0, couldBeToplevel = true) => {
	let isName = str => name == '*' ? str!='' : name == str;
	while(i < ast.length){
	    let fAst = flatten(ast.slice(i, i+8));
	    // let get = n => ast.slice(i, i+45).filter(x => x.kind != SPACE)[n] || {kind: '?', str: '', cclass: ''};
	    let fAstNoSpace = fAst.filter(x => x.kind != SPACE);
	    let get = n => fAstNoSpace[n] || {kind: '?', str: '', cclass: ''};
	    let e = ast[i];
	    
	    if(couldBeToplevel){
		let look = n => (isKW(get(n).str))
		    && isName(get(n+1).str);
		if( look(0)
		    || (isKW(get(0).str) && getf(1).str == "(" && isName(getf(2).str))
		    || ((look(1) || look(2)) && isQUAL(e.str))
		    || ((look(2) || look(3)) && isQUAL(e.str) && isQUAL(get(1).str))		    
		  ){
		    return i;
		}else{
		    console.log(':(', fAst.map(x => x.str).join('').replace(/\n/g,' '));
		}
	    }
	    couldBeToplevel = false;
	    if(e.kind == SPACE && e.str.endsWith('\n'))
		couldBeToplevel = true;
	    i++;
	}
	return false;
    };
    let i = f(name, start);
    if(i === false)
	throw `${name} not found`;
    let j = f('*', i+3, false) || ast.length;
    return {ast: trimAST(ast.slice(i, j)), slice: [i, j]};
};


let DBG_CUR_FILE = '?';

let guessFirstTopLevelName = (ast0) => {
    // ast = ast.filter(x => x.kind == NAME);
    let ast = flatten(ast0);
    let s = (a, b) => trimLeftAST(skipf(trimLeftAST(a), b));
    let x = s(
	s(s(
	    s(
		ast, x => isQUAL(x.str)
	    ),
	    x => isKW(x.str) // let, val...
	), x => x.str == 'rec'),
	x => x.str == '('
    );
    x[0] || console.log('>>>', DBG_CUR_FILE, ast.map(x => x.str));
    x = x[0].str;
    if(x == undefined){
	console.log(ast.map(x => x.str).slice(0,8).join(''), '===>');
	console.log('\t',x);
	console.log(ast);
    }
    return x;
};

let findAnyTopLevel = (ast0, start=0) => {
    try{
	let {ast, slice} = findTopLevel(ast0, '*', start);
	let name = guessFirstTopLevelName(ast);
	return {ast, slice, name};
    }catch(e){
	if(!(e+'').endsWith('not found'))
	    throw e;
	return undefined;
    }
};
let findEveryTopLevel = (ast, start=0) => {
    let l = [];
    let i;
    while((i = findAnyTopLevel(ast, start))){
	let [j, k] = i.slice;
	start = k;
	l.push(i);
    }
    return l;
};

const {basename} = require('path');
let [file, term, where_output, ____] = ((() => {
    let s = process.argv;
    let i = s.findIndex(x => basename(x) == basename(__filename)
			||   basename(x) == 'fst2tex');
    if(i==-1)
	throw "err parsing args";
    return s.slice(1 + i);
})());

if(!file){
    console.log('supply file please');
    process.exit();
}

if(____){
    console.log();
}

if(term == 'false')
    term = undefined;
// console.log({file, term});

let sources = (() => {
    let files = [file];
    if(file.match(/\/\*$/)){
	let path = file.replace(/\/\*$/, '');
	files = fs.readdirSync(path)
	    .filter(x => x.match(/\.fst$/) && !x.match(/#/))
	    .map(x => path + '/' + x);
    }
    return files
	.map(file =>
	     [file,
	      fs.readFileSync(file)+''
	      // (fs.readFileSync(file)+'').replace(/\(\*[\s\S]*?\*\)/gm, '')
	     ]);
})();

// ast = latexify(
//     [
// 	// nRulesMakers.f_a_b_("wp_of_statement"
// 	// 		    , (_, a) => [wrapTex("⟦")
// 	// 				 , a
// 	// 				 , wrapTex("⟧")])
//     ],
//     [
	
// 	removeComments,
// 	// ({kind, cclass, str})  => ({kind, cclass, str:
// 	// 	 str.replace(/^([^\d].*?)(\d+)$/, '$1<sub>$2</sub>')
// 	// 	}),
// 	// ...stuff_replace.map(([w, s]) =>
// 	// 		     tRulesMakers.replaceName(w, s))
//     ],
//     ast); // 

let writeSync = (path, content) => {
    // if(fs.existsSync(path))
    // 	throw path+' exists already!';
    fs.writeFileSync(path, content);
};


let rulesReplTex = [
        ...stuff_replace_tex.map(([w, s]) =>
				  tRulesMakers.replaceName(w, s))
        , tRulesMakers.replaceSubstr(/_/g, '\\_')
	    , tRulesMakers.replaceSubstr(/\|/g, "$\\mid$")
        ];

if(term=="export_all_tex"){
    for(let [file, source] of sources){
	DBG_CUR_FILE = file;
	console.log(file);
	let module = basename(file).replace(/\.fst$/, '');
	let {ast, htmlHead, css} = tokenizeThenParse(source);
	ast = latexify([], [removeComments], ast);
	let names = '';
	for(let decl of findEveryTopLevel(ast)){
	    let m_ast = latexify([], [
		removeComments,
		o => {
		    let str = o.str.replace(/([${_}\\#])/g, '\\$1');
		    return Object.assign(Object.assign({}, o), {str});
		},
		...rulesReplTex
	    ], decl.ast);
	    let output = printLaTeX(m_ast, css);
	    names += `\\input{\\detokenize{decls/${module+'.'+decl.name}}}\n`;
	    writeSync(where_output+'/'+module+'.'+decl.name+'.tex', output);
	}
	writeSync(where_output+'/all.'+module+'.tex', names);
    }
}else{
    // if(!where_output.match(/.tex/))
    let raw = sources.map(([_,src]) => src).join('\n');
    let sha1 = require('crypto').createHash('sha1').update("helloooo").digest('hex');
    try{
	let firstLine = (fs.readFileSync(where_output).toString().split('\n')[0] || '');
	if(firstLine.match(sha1)){
	    console.log('Source has not changed, quit');
	    process.exit();
	}
    }catch(e){

    }
    
    let {ast, htmlHead, css} = tokenizeThenParse(raw);
    ast = latexify([], [removeComments,
			o => {
			    let str = o.str.replace(/[$#]/g, (_, x) => '\\'+x);
			    return Object.assign(Object.assign({}, o), {str});
			},
			...rulesReplTex], ast);
    if(term)
	ast = findTopLevel(ast, term).ast;

    let output = '%' + sha1 + '\n' + printLaTeX(ast, css);
    if(!where_output || where_output == '-')
	console.log(output);
    else
	writeSync(where_output, output);
    // }
}

// console.log(output);

// writeSync('/tmp/hey.html', );

// let x = execSync(`emacs` );

// console.log(x);
// printAST(ast, h)


// console.log();

//.map(({str, kind}) => str+'\t'+kind).join('\n'));
// console.log(JSON.stringify(ast, null, 4));
// console.log(JSON.stringify(parse(tokenize(cc)), null, 2));
